/* ------------------------------------------
 * Global Vars
-------------------------------------------*/
var mvMatrix = mat4.create();
var pMatrix = mat4.create();

var ANGLE_STEP = 45.0;
var then = Date.now();

function main() {
  // Retrieve <canvas> element
  var canvas = document.getElementById("webgl");
  if (!canvas) {
	console.log("Failed to load canvas element");
	return;
  }

  // Get the rendering context for WebGL
  var gl = getWebGLContext(canvas);
  if (!gl) {
	console.log("Failed to get WebGL context");
	return;
  }

  // Initialize shaders
  var vShaderSource = getShaderSource("shader-vs");
  var fShaderSource = getShaderSource("shader-fs");
  if (!initShaders(gl, vShaderSource, fShaderSource)) {
	console.log("Failed to initialize shaders");
	return;
  }

  // Set the clear color and enable the depth test
  gl.viewportWidth = canvas.width;
  gl.viewportHeight = canvas.height;

  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  gl.enable(gl.DEPTH_TEST);

  // Get the storage locations of attribute and uniform variables
  var program = gl.program;
  program.a_Position = gl.getAttribLocation(program, 'a_position');
  // program.a_Color = gl.getAttribLocation(program, 'a_color');
  program.a_textureCoord = gl.getAttribLocation(program, 'a_textureCoord');

  program.u_mvMatrix = gl.getUniformLocation(program, 'u_mvMatrix');
  program.u_pMatrix = gl.getUniformLocation(program, 'u_pMatrix');

  program.u_sampler = gl.getUniformLocation(program, "u_sampler");

  if (program.a_Position < 0 || /*program.a_Color < 0 ||*/
	  program.a_textureCoord < 0 || !program.u_sampler ||
	  !program.u_mvMatrix || !program.u_pMatrix) {
	console.log('attribute, uniform locations failed to load'); 
	return;
  }

  $.getJSON('teapot_0.json', function(g_json) {
	// parse the json file
	g_data = parse_json(g_json);

	// load and setup the texture
	init_textures(gl);

	// initialize buffers using parsed data
	init_buffers(gl, g_data);

	// draw geometry
	var rate = 0.0;
	render();

	function render() {
	  requestAnimationFrame(render);
	  rate = animate(rate);
	  drawScene(gl, g_data, rate);
	}
  });
}

/* ------------------------------------------
 * Initialize Texture
-------------------------------------------*/
function init_textures(gl) {
  var program = gl.program;

  program.texture_buffer = gl.createTexture();
  var texture_image = new Image();
  texture_image.onload = function() {
	handleTextureLoaded(gl, texture_image, program.texture_buffer);
  };
  texture_image.src = "cubetexture.png";
}

/* ------------------------------------------
 * Handle loaded image
-------------------------------------------*/
function handleTextureLoaded(gl, image, texture) {
  gl.bindTexture(gl.TEXTURE_2D, texture);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA,
	  gl.UNSIGNED_BYTE, image);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,
	  gl.LINEAR_MIPMAP_NEAREST);
  gl.generateMipmap(gl.TEXTURE_2D);
  gl.bindTexture(gl.TEXTURE_2D, null);
}

/* ------------------------------------------
 * Parse JSON
-------------------------------------------*/
function parse_json(json) {
  var v3 = vec3.create;

  // data to be passed to buffers for drawing
  var o = {};
  // o.colors = [];
  o.indices = [];
  o.vertices = [];
  o.vertex_normals = [];
  o.texture_coords = [];

  // vertex normal accumulator
  var acc = {};

  // parse indices
  for (var i = 0; i < json.faces.length; i++) {
	var face = json.faces[i];

	// face vertices
	var vrt1 = json.vertices[face[0]];
	var vrt2 = json.vertices[face[1]];
	var vrt3 = json.vertices[face[2]];

	// face vectors
	var v1 = vec3.sub(v3(), vrt2, vrt1);
	var v2 = vec3.sub(v3(), vrt3, vrt1);

	// face normal
	var n = vec3.normalize(v3(), vec3.cross(v3(), v1, v2));

	for (var ii = 0; ii < face.length; ii++) {
	  var v_idx = face[ii];

	  var acc_vrt_nrm = (acc[v_idx] || v3());
	  acc[v_idx] = vec3.add(v3(), acc_vrt_nrm, n);
	}

	o.indices = o.indices.concat(face);
  }

  var y_max = 0;
  for (var k = 0; k < json.vertices.length; k++) {
	var candidate_y = json.vertices[k][1];
	if (candidate_y > y_max) {
	  y_max = candidate_y;
	}
  }

  // parse vertices, normals and colors
  for (var j = 0; j < json.vertices.length; j++) {
	var vertex = json.vertices[j];

	// save vertex in format webgl can understand
	o.vertices = o.vertices.concat(vertex);

	// grab accumulated vertex normal, normalize, and store
	var vn = vec3.normalize(v3(), acc[j]);

	o.vertex_normals = o.vertex_normals.concat([vn[0], vn[1], vn[2]]);

	// compute texture coordinates
	var theta = Math.atan2(vertex[2], vertex[0]);
	var s = (theta + Math.PI) / (2 * Math.PI);
	var t = vertex[1] / y_max;

	o.texture_coords = o.texture_coords.concat([s, t]);

	// save same color for each vertex
	// o.colors = o.colors.concat([0.8, 0.8, 0.8]);
  }

  return o;
}

/* ------------------------------------------
 * Init buffers
-------------------------------------------*/
function init_buffers(gl, data) {
  var program = gl.program;
  console.log(data);

  // buffer for geometry vertices
  var vertex_buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vertex_buffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.vertices),
	  gl.STATIC_DRAW);
  // sent as triple for (x, y, z)
  gl.vertexAttribPointer(program.a_Position, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(program.a_Position);

  // buffer for geometry texture coordinates
  var texture_coords_buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, texture_coords_buffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.texture_coords),
	  gl.STATIC_DRAW);
  // sent as double for (s, t)
  gl.vertexAttribPointer(program.a_textureCoord, 2, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(program.a_textureCoord);

  /*
  // buffer for geometry colors
  var color_buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, color_buffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data.colors),
	  gl.STATIC_DRAW);
  // sent as triple for (r, g, b)
  gl.vertexAttribPointer(program.a_Color, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(program.a_Color);
  */

  // buffer for geometry indices
  var index_buffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, index_buffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(data.indices),
	  gl.STATIC_DRAW);
}

/* ------------------------------------------
 * Draw the scene
-------------------------------------------*/
function drawScene(gl, g_data, rate) {
  var program = gl.program;

  gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  mat4.perspective(pMatrix, 45, gl.viewportWidth / gl.viewportHeight,
	0.1, 100.0);

  mat4.identity(mvMatrix);

  // apply simple rotation about y axis to camera
  var d = 6;
  var dx = d * Math.cos(degToRad(rate));
  var dz = d * Math.sin(degToRad(rate));

  var eye = [dx, d, dz];
  var center = [0, 1, 0];
  var up = [0, 1, 0];

  mat4.lookAt(mvMatrix, eye, center, up);

  // activate textures
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, program.texture_buffer);
  gl.uniform1i(program.u_sampler, 0);

  setMatrixUniforms(gl);

  gl.drawElements(gl.TRIANGLES, g_data.indices.length,
	  gl.UNSIGNED_SHORT, 0);
}

/* ------------------------------------------
 * Uniform Matrix Logic
-------------------------------------------*/
function setMatrixUniforms(gl) {
  var program = gl.program;
  gl.uniformMatrix4fv(program.u_mvMatrix, false, mvMatrix);
  gl.uniformMatrix4fv(program.u_pMatrix, false, pMatrix);
}

/* ------------------------------------------
 * Animation Logic
-------------------------------------------*/
function animate(current_angle) {
  var now = Date.now();
  var elapsed = now - then;
  then = now;

  return (current_angle + (elapsed * ANGLE_STEP) / 1000.0) % 360;
}

/* ------------------------------------------
 * Retrieve shader source logic
-------------------------------------------*/
function getShaderSource(id) {
  var shaderScript = $('#' + id);
  if (!shaderScript.get(0)) {
	return null;
  }

  return shaderScript.html();
}

/* ------------------------------------------
 * Simple degree to radians function
-------------------------------------------*/
function degToRad(degree) {
  return degree * (Math.PI / 180.0);
}
