var fs = require('fs');
var lineReader = require('line-reader');

if (!process.argv[2]) {
  console.log("Correct usage is 'node parse_obj.js <obj file>'");
  return;
}

var supported_types = {'v': "vertices", 'f': "faces"};
var inputFile = process.argv[2];
var geometry = {};

lineReader.eachLine(inputFile, function (line, last) {
  parse_line(line);

  if (last) {
	// Write parsed data to JSON file
	var outputFile = './' + inputFile.split('.')[0] + ".json";
	var b_json = JSON.stringify(geometry, null, 2);

	fs.writeFile(outputFile, b_json, function (err) {
	  if (err) {
		console.log(err);
	  }

	  console.log("JSON saved to " + outputFile);
	});
  }
});

function parse_line(line) {
  var l = line.match(/\S+/g);
  var type = l[0];

  if (type === 'v' || type === 'f') {
	type = supported_types[type];
	var data = l.slice(1);

	parse_data(type, data);
  }
}

function parse_data(type, data) {
  data = data.map(Number);

  if (type === 'faces') {
	data[0] -= 1;
	data[1] -= 1;
	data[2] -= 1;
  }

  var b_array = geometry[type];

  if (!b_array) {
	b_array = geometry[type] = [];
  }

  b_array.push(data);
}
